#!/bin/bash

num_containers=$(docker ps | wc -l)
if [[ num_containers -gt 1 ]]; then
    echo "Project is running, starting to wipe it"
    ./destoy_project.sh
fi
echo "Starting new build."
docker compose build
docker compose up -d  ; docker compose logs -f